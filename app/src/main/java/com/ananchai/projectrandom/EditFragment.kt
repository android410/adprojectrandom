package com.ananchai.projectrandom

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import androidx.navigation.findNavController

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ananchai.projectrandom.databinding.FragmentEditBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var recyclerView: RecyclerView

    private lateinit var list: List<Random>
    private  var _binding: FragmentEditBinding? = null
    private  val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentEditBinding.inflate(inflater, container, false)
        return _binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(context)

        recyclerView = view.findViewById(R.id.showList)
        recyclerView.layoutManager = layoutManager
        list = RandomSouce().loadData()
        recyclerView.adapter = RandomAdapter(list)

        binding.backToRandom.setOnClickListener{
            val action = EditFragmentDirections.actionEditFragmentToRandomFragment()
            view.findNavController().navigate(action)
        }

        binding.goToAdd.setOnClickListener{
            binding.showList
            val action = EditFragmentDirections.actionEditFragmentToAddFragment()
            view.findNavController().navigate(action)
        }

        binding.showList.layoutManager = LinearLayoutManager(this.context)


        binding.goToDelete.setOnClickListener{
            if(allWordList.size == 0){
                Toast.makeText(context, "Please Add Data", Toast.LENGTH_SHORT).show()
            }
            else{
                allWordList.removeLast()
                recyclerView = view.findViewById(R.id.showList)
                recyclerView.layoutManager = layoutManager
                list = RandomSouce().loadData()
                recyclerView.adapter = RandomAdapter(list)
            }

        }

    }



    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}