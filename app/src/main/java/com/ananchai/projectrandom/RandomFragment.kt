package com.ananchai.projectrandom

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ananchai.projectrandom.databinding.FragmentRandomBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RandomFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RandomFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val hendler = Handler()

    private var showRandomWord = "click random"

    private  var _binding: FragmentRandomBinding? = null
    private  val binding get() = _binding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRandomBinding.inflate(inflater, container, false)
        return _binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateNextWordOnScreen()
        binding?.random?.setOnClickListener {
            if(allWordList.size == 0){
                Toast.makeText(context, "Please Add Data", Toast.LENGTH_SHORT).show()
            }
            else{
                onRandomWord()
            }

        }


        binding?.goEdit?.setOnClickListener{
//            allWordMutable.add("3")
//            allWordMutable.add("4")
//            allWordMutable.add("5")
            val action = RandomFragmentDirections.actionRandomFragmentToEditFragment()
            view.findNavController().navigate(action)
        }


    }
    private fun onRandomWord() {
        showRandomWord = ""
        updateNextWordOnScreen()

        binding?.balloon?.alpha = 0.0f
        binding?.ltAnimation?.alpha = 1.0f

        showRandomWord = getNextScrambledWord()

        hendler.postDelayed({binding?.ltAnimation?.alpha = 0.0f},3000)
        hendler.postDelayed({updateNextWordOnScreen()},3000)
        hendler.postDelayed({binding?.balloon?.alpha = 1.0f},3000)
    }

    private fun getNextScrambledWord(): String {

        val tempWord = allWordList.random()
        return tempWord.name
    }



    private fun updateNextWordOnScreen() {
        binding?.textViewRandomWord?.text = showRandomWord
    }



    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RandomFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RandomFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}