package com.ananchai.projectrandom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RandomAdapter(private val dataset: List<Random>): RecyclerView.Adapter<RandomAdapter.ItemViewHolder>() {
    class ItemViewHolder(private var binding: View): RecyclerView.ViewHolder(binding) {
        val textView: TextView = binding.findViewById(R.id.nameRandom)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = item.name

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }
}